const express = require("express");
const router = express.Router();
const fileService = require("../services/fileService");

// @desc   Create file with params
// @route  POST /api/files
router.post('/', (req, res) => {	
	const {password} = req.query;

  fileService.checkParams(req.body);
  fileService.createFile(req.body, password);
  res.status(200).json({ message: "File created successfully" });
});

// @desc   Get list of all uploaded files as array of filenames
// @route  GET /api/files
router.get('/', (req, res) => {
  const files = fileService.getFiles();

  res.status(200).json({
    message: "Success",
    files,
  });
});

// @desc   Get file detailed info by specified filename
// @route  GET /api/files/:filename
router.get('/:filename', (req, res) => {
	const {filename} = req.params;
	const {password} = req.query;	

  const fileInfo = fileService.getFileInfo(filename, password);

  res.status(200).json(fileInfo);
});

// @desc   Update existing file (bulk update)
// @route  PUT /api/files/:filename
router.put('/:filename', (req, res) => {
	const {filename} = req.params;
	const oldPassword = req.query.password;
	const {content, password} = req.body;

	fileService.updateFile(filename, content, oldPassword, password);
	res.status(200).json({ message: `File updated successfully` });
});

// @desc   Delete file
// @route  DELETE /api/files/:filename
router.delete('/:filename', (req, res) => {
	const {filename} = req.params;
	const {password} = req.query;	

	fileService.deleteFile(filename, password);
	res.status(200).json({ message: `File deleted successfully` });
});

module.exports = router;
