const ApiError = require('./utils/ApiError');
const logger = require('./utils/logger')

function apiErrorHandler(err, req, res, next) {     
    if (err instanceof ApiError) {        
        logger.info(err);
        res.status(err.code).json({message: err.message});
        return;
    }

    logger.error(err);
    res.status(500).json({message: 'Server error'});    
}

module.exports = apiErrorHandler;