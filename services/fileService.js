const fs = require('fs');
const path = require('path');
const ApiError = require('../middleware/utils/ApiError');

const extensions = ['.log', '.txt', '.json', '.yaml', '.xml', '.js'];
const folderPath = path.join(__dirname, "../files/");

try {
  if (!fs.existsSync(folderPath)) {
    fs.mkdirSync(folderPath);
  }
} catch (error) {
  throw ApiError.internal(`Cannot create /files folder`);
}

const fileService = {
  createFile: function ({ filename, content }, password) {
    const filePath = path.join(folderPath, filename);

    // Check if file already exists
    const fileExists = fs.existsSync(filePath);

    if (fileExists) {
      throw ApiError.internal(`File '${filename}' already exists`);
    }

    this.writeFile(filePath, content, password);
  },

  getFiles: function () {
    return fs.readdirSync(folderPath);
  },

  getFileInfo: function (filename, requestPassword) {
    const filePath = path.join(folderPath, filename);

    // Check if file exists
    this.checkIfFileExists(filePath);

    const fileJSON = fs.readFileSync(filePath, { encoding: 'utf-8' });
    const { content, password } = JSON.parse(fileJSON);

    // Check password
    this.checkPassword(password, requestPassword);

    const extension = path.extname(filename).slice(1);
    const uploadedDate = fs.statSync(filePath).birthtime;

    return {
      message: "Success",
      filename,
      content,
      extension,
      uploadedDate,
    };
  },

  updateFile: function (filename, content, oldPassword, newPassword) {
    const filePath = path.join(folderPath, filename);

    if (newPassword && typeof newPassword !== 'string') {
      throw ApiError.badRequest(
        `Fail to set a new password. Password have to be of type 'string'`
      );
    }

    // Check if file exists
    this.checkIfFileExists(filePath);

    const fileJSON = fs.readFileSync(filePath, { encoding: 'utf-8' });
    const { password } = JSON.parse(fileJSON);

    // Check password
    this.checkPassword(password, oldPassword);

    // Check 'content' parameter
    this.checkContent(content);

    if (newPassword) {
      this.writeFile(filePath, content, newPassword);
      return;
    }

    this.writeFile(filePath, content, oldPassword);
  },

  deleteFile: function (filename, requestPassword) {
    const filePath = path.join(folderPath, filename);

    // Check if file exists
    this.checkIfFileExists(filePath);

    const fileJSON = fs.readFileSync(filePath, { encoding: 'utf-8' });
    const { password } = JSON.parse(fileJSON);

    // Check password
    this.checkPassword(password, requestPassword);

    fs.rmSync(filePath);
  },

  checkParams: function ({ filename, content }) {
    // Check that 'filename' parameter exists and it isn't empty string
    if (!filename) {
      throw ApiError.badRequest(`Please specify 'filename' parameter`);
    }

    // Check that 'filename' parameter is of type 'string'
    if (typeof filename !== 'string') {
      throw ApiError.badRequest(
        `'filename' parameter should be of type 'string'`
      );
    }

    // Check 'content' parameter
    this.checkContent(content);

    // Check that filename has extension
    const fileExtension = path.extname(filename);

    if (!fileExtension) {
      throw ApiError.badRequest(`Please specify file extension`);
    }

    // Check that filename extension is allowed
    if (!extensions.includes(fileExtension)) {
      throw ApiError.badRequest(`${fileExtension} extension is not allowed`);
    }
  },

  checkContent: function (content) {
    // Check that 'content' parameter exists and it isn't empty string
    if (!content) {
      throw ApiError.badRequest(`Please specify 'content' parameter`);
    }

    // Check that 'content' parameter is of type 'string'
    if (typeof content !== `string`) {
      throw ApiError.badRequest(`'content' parameter should be of type 'string'`);
    }
  },

  checkIfFileExists: function (filePath) {
    const fileExists = fs.existsSync(filePath);

    if (!fileExists) {
      filename = path.basename(filePath);
      throw ApiError.internal(`No file with '${filename}' filename found`);
    }
  },

  checkPassword: function (password, requestPassword) {
    if (password) {
      if (!requestPassword) {
        throw ApiError.internal(
          `Unauthorized access. File is protected by password`
        );
      }
      if (password !== requestPassword) {
        throw ApiError.badRequest(`Not correct password`);
      }
    }
  },

  writeFile: function (filePath, content, password) {
    if (password) {
      fs.writeFileSync(filePath, JSON.stringify({ content, password }));
      return;
    }

    fs.writeFileSync(filePath, JSON.stringify({ content }));
  },
};

module.exports = fileService;
