const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const apiErrorHandler = require('./middleware/errorHandler');

// Load config
dotenv.config({path: './config/config.env'});

const app = express();

// Body parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Logging
app.use(morgan('dev'));

// Set Routes
app.use('/api/files', require('./routes/router'));

// Error handling
app.use(apiErrorHandler);

const PORT = process.env.PORT;

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
})

